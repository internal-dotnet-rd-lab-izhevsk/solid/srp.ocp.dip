﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Epam.Rd.Application.Interfaces;
using Epam.Rd.Application.Models;

namespace Epam.Rd.Application.Services
{
	public class NotificationService : INotificationService
	{
		private readonly EmailService _emailService = new();
		private readonly FileStorage _fileStorage = new();

		private const string BodyFooter = "С уважением, команда .NET УЦ, г. Ижевск";
		private const string ContactInformation = "rd.net@epam.com";

		public Notification CreateNotificationModuleCompleted(User student, User mentor, string moduleName)
		{
			var subject = $"Модуль {moduleName} был успешно завершен";
			var message = $"Информируем вас о том, что студент {student.FullName} успешно завершил изучение модуля {moduleName} {DateTime.UtcNow:dd.MM.yyyy HH:mm}";
			var users = new[] {student, mentor};
			var body = GetBody(message, users);
			var notification = new Notification
			{
				Subject = subject,
				Body = body,
				To = users
			};

			return notification;
		}

		public Notification CreateNotificationModuleIsOverdue(User student, User mentor, string moduleName, DateTime deadline)
		{
			var subject = $"Модуль {moduleName} не завершен вовремя";
			var message = $"Информируем вас о том, что студент {student.FullName} не завершил модуль {moduleName} в установленный срок {deadline:dd.MM.yyyy HH:mm}";
			var users = new[] {student, mentor};
			var body = GetBody(message, users);
			var notification = new Notification
			{
				Subject = subject,
				Body = body,
				To = users
			};

			return notification;
		}

		public Notification CreateNotificationNewModuleActivated(User student, User mentor, string moduleName, DateTime deadline)
		{
			var subject = $"Модуль {moduleName} добавлен в план обучения";
			var message = $"Информируем вас о том, что в учебном плане студента {student.FullName} был активирован новый модуль {moduleName}, срок выполнения: {deadline:dd.MM.yyyy HH:mm}";
			var users = new[] {student, mentor};
			var body = GetBody(message, users);
			var notification = new Notification
			{
				Subject = subject,
				Body = body,
				To = users
			};

			return notification;
		}

		public Notification CreateNotificationForAdminErrorInModuleFound(User coordinator, string moduleName, string description)
		{
			var subject = $"Обнаружена ошибка в модуле {moduleName}";
			var body = $"В модуле {moduleName} была обнаружена ошибка.<br/>Детали ошибки:<br/>{description}";
			var notification = new Notification
			{
				Subject = subject,
				Body = body,
				To = new[] {coordinator}
			};

			return notification;
		}

		public Notification CreateNotificationForAdminModuleMaterialsAreInaccessible(User coordinator, string moduleName)
		{
			var subject = $"Материалы модуля {moduleName} недоступны";
			var body = $"Материалы модуля {moduleName} в настоящее время недоступны.";
			var notification = new Notification
			{
				Subject = subject,
				Body = body,
				To = new[] {coordinator}
			};

			return notification;
		}

		public void SendNotification(Notification notification)
		{
			var email = new MailMessage
			{
				From = new MailAddress("rd.net@epam.com", ".NET УЦ, г. Ижевск"),
				To = {string.Join(',', notification.To)},
				Body = notification.Body,
				Subject = notification.Subject
			};

			_emailService.SendEmail(email);
		}

		public void SaveAndSendNotification(Notification notification)
		{
			_fileStorage.Save(notification);
			SendNotification(notification);
		}

		private string GetBody(string message, IEnumerable<User> to)
		{
			var bodyBuilder = new StringBuilder();
			bodyBuilder.Append($"Добрый день, {string.Join(", ", to.Select(u => u.FullName))}.");
			bodyBuilder.Append("<br/>");
			bodyBuilder.Append("<br/>");
			bodyBuilder.Append(message);
			bodyBuilder.Append("<br/>");
			bodyBuilder.Append("<br/>");
			bodyBuilder.Append(BodyFooter);
			bodyBuilder.Append("<br/>");
			bodyBuilder.Append(ContactInformation);

			return bodyBuilder.ToString();
		}
	}
}